#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define M 3
#define N 5

int E[M];
int A[M];

int C[N][M];
int R[N][M];

int closed[N];

void read_input(FILE* file) {
    for (int i=0; i<M; i++) {
        fscanf(file, "%d", &E[i]);
    }
    
    for (int i=0; i<M; i++) {
        fscanf(file, "%d", &A[i]);
    }

    for (int i=0; i<N; i++) {
        for (int j=0; j<M; j++) {
            fscanf(file, "%d", &C[i][j]);
        }
    }

    for (int i=0; i<N; i++) {
        for (int j=0; j<M; j++) {
            fscanf(file, "%d", &R[i][j]);
        }
    }
}


void open_processes() {
    for(int i=0; i<M; i++) {
        closed[i] = 0;
    }
}

void close_process(int x) {
    for(int i=0; i<M; i++) {
        A[i] += C[x][i] + R[x][i];
    }
    closed[x] = 1;
}

int all_closed(){
    for(int i=0; i<M; i++) {
        if (closed[i]!=1) return 0;
    }
    return 1;
}

int search_resources() {
    if (all_closed()==1) return 1;

    for (int i=0; i<N; i++) {
        if (closed[i] == 1) continue;
    
        int available = 1;
        for (int j=0; j<M; j++) {
            if (R[i][j] > A[j]) {
                available=0;
                break;
            } 
        }
        if (available==1) {
            close_process(i);
            return search_resources();
        }
    }

    return 0;
}

int main() {
    int num;
    FILE *input_file;

    input_file = fopen("input_dl.txt", "r");

    if (input_file == NULL) {
        printf("Error!");   
        exit(1);             
    }

    read_input(input_file);
    open_processes();
    if (search_resources()==0) {
        printf("Deadlock!");   
    } else {
        printf("Ok!");   
    }

    fclose(input_file);

	return 0;
}

