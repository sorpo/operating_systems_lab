#include <stdio.h>
#include <linux/input.h>
#include <dirent.h>

int main (void)
{
    const char* file = fopen("/dev/input/by-path/platform-i8042-serio-0-event-kbd", "rb");
    struct input_event event;

    if (file == NULL) {
        printf("The file '/dev/input/by-path/platform-i8042-serio-0-event-kbd' doesn't exist");
        return -1;
    }

    while (fread((void*) &event, sizeof(struct input_event), 1, file)) {
        if (event.type == EV_KEY){
            if (event.value == 0) {
                printf("RELEASED ");
            }
            else if (event.value == 1) {
                printf("PRESSED ");
            } else if (event.value ==2){
                printf("REPEATED ");
            }
            printf("0x%04x (%d)\n", event.code, event.code);
        }
    }

    fflush(stdout);
    return 0;
}
