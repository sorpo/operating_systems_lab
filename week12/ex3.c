#include <stdio.h>
#include <linux/input.h>
#include <dirent.h>

#define NUM_OF_KEYS = 3;
int keys[NUM_OF_KEYS];

void add_key(int key) {
    for (int i = NUM_OF_KEYS; i > 0; i--) {
        keys[i] = keys[i-1];
    }
keys[0] = key;
}

int main (void)
{
    struct input_event event;

    for (int i = 0; i < NUM_OF_KEYS; i++) {
        keys[i] = -1;
    }

    const char* file = fopen("/dev/input/by-path/platform-i8042-serio-0-event-kbd", "rb");
    if (file == NULL) {
        printf("The file '/dev/input/by-path/platform-i8042-serio-0-event-kbd' doesn't exist");
        return -1;
    }

    while (fread((void*) &event, sizeof(struct input_event), 1, file)) {
        if (event.type == EV_KEY){
            if (event.value == 1) {
                printf("PRESSED ");
                for (int i = NUM_OF_KEYS; i > 0; i--) {
                    keys[i] = keys[i-1];
                    printf("%d ", keys[i])
                }
                printf("/n");
                keys[0] = key;
            } else {
                continue;
            }

            // P E
            if (keys[0] == 18 && 
                keys[1] == 25) printf("I passed the Exam!\n");
            

            // C A P
            if (keys[0] == 25 && 
                keys[1] == 30 && 
                keys[2] == 46) printf("Get some cappuccino!\n");
            
            // H I
            if (keys[0] == 23 && 
                keys[1] == 35) printf("Hello World!\n");
        }
    }

    fflush(stdout);


    return 0;
}