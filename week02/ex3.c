#include <stdio.h>

void printTree(int);
void printSpaces(int);


int main() {
    int n;
    scanf("%d", &n);
    printTree(n);

    return 0;
    
}

void printTree(int n) {
    int leftSpaces = 2*n;

    for (int i = 1; i <= n; i++) {
        printSpaces(leftSpaces - 2*i);

        for (int j = 0; j < 2*i - 1; j++) {
            printf("* ");
        }

        printf("\n");
    }
}

void printSpaces(int n) {
    for (int i = 0; i < n; i++) {
        printf(" ");
    }
}
