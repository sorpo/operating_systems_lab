#include <stdio.h>
#include <string.h>


void printReverse(char*);

int main() {
    char string[100];
    printf("Input:\n");
    scanf("%s", string);
    printf("Reversed:\n");
    printReverse(string);

    return 0;
}

void printReverse(char* s) {
    int len = strlen(s);
    for (int i = len - 1; i >= 0; i--) {
        putchar(s[i]);
    }
}