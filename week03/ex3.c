#include <stdio.h>
#include <stdlib.h>

struct Node {
    int value;
    struct Node* next;
};
typedef struct Node* List;

List create_node(int);
void print_list(List*);
void insert_node(List*, List);
void delete_node(List*, List);

int main() {
    List head = create_node(0);
    printf("Create 0 node: ");
    print_list(&head);

    List second = create_node(1);
    insert_node(&head, second);
    printf("Insert 1: ");
    print_list(&head);

    delete_node(&head, second);
    printf("Delete 1: ");
    print_list(&head);

    return 0;

}

List create_node(int value) {
    List node = (List) malloc(sizeof(struct Node));
    node->value = value;
    node->next = NULL;
    return node;
}

void print_list(List* head) {
    List current = *head;
    while (current != NULL) {
        printf("%d ", current->value);
        current = current->next;
    }
    putchar('\n');
}

void insert_node(List* tail, List node) {
    // If tail doesn't exist
    if (*tail == NULL) {
        return;
    }
    // If tail is the last element in the list
    if ((*tail)->next == NULL) {
        (*tail)->next = node;
    }
    else {
        node->next = (*tail)->next;
        (*tail)->next = node;
    }
}

void delete_node(List* head, List node) {
    if (*head == node) {
        node->next = NULL;
        return;
    }
    List current = *head;
    List previous;

    while (current != NULL && current != node) {
        previous = current;
        current = current->next;
    }

    if (current == NULL) {
        return;
    }

    previous->next = current->next;
    current->next = NULL;
}