#include <stdio.h>
#include <stdlib.h>

const int MAX_PROCESSES = 10;

typedef struct
{
    int id;
    int arrival_time;
    int burst_time;
    int completion_time;
    int waiting_time;
    int turnaround_time;
} process_metrics;


process processes [MAX_PROCESSES];
int average_waiting_time;
int average_turnaround_time;
int N;

int max(int b, int a);

int at_comparator(const void *a, const void *b) {
    process pa = *(const process *)a;
    process pb = *(const process *)b;

    return pa.burst_time - pb.burst_time;
}


void input_info () {
    printf("Enter the number of processes (up to %d):\n ", MAX_PROCESSES);
    scanf("%d",&N);

    for(int i=0; i<N; i++) {
        printf("Process %d\n", i);
        processes[i].id = i;

        printf("Enter the arrival time: ");
        scanf("%d", &processes[i].arrival_time);

        printf("Enter the burst time: ");
        scanf("%d",&processes[i].burst_time);
    }
}

void calc_process_info() {
    int total_waiting_time = 0;
    int total_turnaround_time = 0;
    qsort(processes, N, sizeof(process), fcfs_comparator);
    processes[0].waiting_time = 0;
    for(int i=1; i<N; i++) {
        processes[i].waiting_time = processes[i - 1].waiting_time + processes[i - 1].burst_time
                                    + processes[i - 1].arrival_time - processes[i].arrival_time;
        processes[i].waiting_time = max(processes[i].waiting_time, 0);

        processes[i].turnaround_time = processes[i].burst_time + processes[i].waiting_time;
        processes[i].completion_time = processes[i].turnaround_time + processes[i].arrival_time;

        total_waiting_time+= processes[i].waiting_time;
        total_turnaround_time+= processes[i].turnaround_time;
    }

    average_waiting_time = total_waiting_time/N;
    average_turnaround_time = total_turnaround_time/N;
}

int max(int b, int a) {
    if (a>b) return a;
    return b;
}

void print_metrics() {
    printf("Average waiting time: %d\n\n", average_waiting_time);
    printf("Average turnaround time: %d\n\n", average_turnaround_time);

    for(int i=0; i<N; i++) {
        printf("Process %d ||\t", processes[i].id);

        printf("ct: %d\t", processes[i].completion_time);
        printf("wt: %d\t", processes[i].waiting_time);
        printf("tat: %d\n\n", processes[i].turnaround_time);
    }
}

int main()
{
    input_info();
    calc_process_info();
    print_metrics();

    return 0;
}




