#include <stdio.h>
#include <string.h>
#include <stdlib.h>



int main()
{
    int memory[10000] = {};
    int counters[10000] = {};
    int r_bits[10000] = {};
    int MEMORY_SIZE;
    int ACCESSES;
    scanf("%d %d", &MEMORY_SIZE, &ACCESSES);

    for(int j = 0; j < MEMORY_SIZE; j++) {
        memory[j] = 0;
        counters[j] = 0;
        r_bits[j] = 0;
    }

    double hitcount = 0;
    int old_clock_cycle = 0;
    for(int i=0; i<ACCESSES; i++) {
//        printf("access %d\n", i);
//        printf("MEMORY:\n");
//        for(int j = 0; j < MEMORY_SIZE; j++) {
//            printf("page: %d, counter: %d\n", memory[j], counters[memory[j]]);
//        }
//        printf("\n");

        int page_number;
        int clock_cycle;
        scanf("%d%d", &clock_cycle, &page_number);

        if(old_clock_cycle != clock_cycle) {
            old_clock_cycle = clock_cycle;

            for(int j = 0; j < MEMORY_SIZE; j++) {
//                printf("%d", r_bits[memory[j]]);
                counters[j] = (counters[j] >> 1) + (r_bits[j] << 7);
                r_bits[j] = 0;
            }
        }

        int hit = 0;
        for(int j = 0; j < MEMORY_SIZE; j++) {
            if (memory[j] == page_number) {
                hit = 1;
                hitcount++;
                printf("1\n");
                r_bits[j] = 1;
                break;
            }
        }

        if(!hit) {
            printf("0\n");
            int nfu_page_index = 0;
            for(int j = 0; j < MEMORY_SIZE; j++) {
                if (memory[j] == 0) {
                    nfu_page_index = j;
                    break;
                }

                if (counters[nfu_page_index] > counters[j]) {
                    nfu_page_index = j;
                } else if (counters[nfu_page_index] == counters[j]) {
                    if (memory[nfu_page_index] > memory[j]) {
                        nfu_page_index = j;
                    }
                }
            }

            memory[nfu_page_index] = page_number;
            counters[nfu_page_index] = 0;
            r_bits[nfu_page_index] = 1;
        }
    }
    printf("%f", hitcount/(ACCESSES-hitcount));


    return (0);
}