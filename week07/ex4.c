//Write your own realloc() function using malloc() and free()
//realloc() changes the size of the memory block pointed to by ptr to
//size bytes. The contents will be unchanged in the range from the
//start of the region up to the minimum of the old and new sizes.
//Newly allocated memory will be uninitialized

//Unless ptr is NULL, it must have been returned by an earlier call to
//        malloc(), calloc() or realloc()


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

void *my_realloc(void *ptr, size_t new_size) {
    void *new_array = malloc(new_size);

    //If ptr is NULL, the call is equivalent to malloc(size)
    if (ptr == NULL) {
        return new_array;
    }

    //If size is equal to zero, the call is equivalent to free(ptr)
    if (new_size == 0) {
        free(ptr);
        return NULL;
    }

    memmove(new_array, ptr, new_size);
    free(ptr);
    return new_array;
}

void test(int initial_size, int new_size) {
    int *a = (int*)malloc(sizeof(int) * initial_size);

    printf("Old size: %d\n", initial_size);
    for (int i = 0; i < initial_size; ++i) {
        a[i] = i;
        printf("%d ", a[i]);
    }
    printf("\n");


    a = my_realloc(a, sizeof(int) * 4);

    for (int i = initial_size; i < new_size; ++i) {
        a[i] = i;
    }

    printf("New size: %d\n", new_size);
    for (int i = 0; i < new_size; ++i) {
        a[i] = i;
        printf("%d ", a[i]);
    }
    printf("\n\n");

    free(a);
}

int main()
{
    test(2, 4);
    test(4, 2);
    test(4, 0);


}