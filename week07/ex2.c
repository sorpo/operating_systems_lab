#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    printf("Enter n: ");
    scanf("%d", &n);

    int* a = (int*) malloc(n * sizeof(int));

    printf("Memory is allocated\n");

    for (int i = 0; i < n; ++i) {
        a[i] = i;
    }


    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }

    free(a);
    printf("\nMemory is deallocated\n");

    return 0;
}